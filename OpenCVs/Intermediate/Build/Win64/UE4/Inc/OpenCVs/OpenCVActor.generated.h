// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPENCVS_OpenCVActor_generated_h
#error "OpenCVActor.generated.h already included, missing '#pragma once' in OpenCVActor.h"
#endif
#define OPENCVS_OpenCVActor_generated_h

#define OpenCVs_Source_OpenCVs_OpenCVActor_h_21_SPARSE_DATA
#define OpenCVs_Source_OpenCVs_OpenCVActor_h_21_RPC_WRAPPERS
#define OpenCVs_Source_OpenCVs_OpenCVActor_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define OpenCVs_Source_OpenCVs_OpenCVActor_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOpenCVActor(); \
	friend struct Z_Construct_UClass_AOpenCVActor_Statics; \
public: \
	DECLARE_CLASS(AOpenCVActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OpenCVs"), NO_API) \
	DECLARE_SERIALIZER(AOpenCVActor)


#define OpenCVs_Source_OpenCVs_OpenCVActor_h_21_INCLASS \
private: \
	static void StaticRegisterNativesAOpenCVActor(); \
	friend struct Z_Construct_UClass_AOpenCVActor_Statics; \
public: \
	DECLARE_CLASS(AOpenCVActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OpenCVs"), NO_API) \
	DECLARE_SERIALIZER(AOpenCVActor)


#define OpenCVs_Source_OpenCVs_OpenCVActor_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOpenCVActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOpenCVActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOpenCVActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOpenCVActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOpenCVActor(AOpenCVActor&&); \
	NO_API AOpenCVActor(const AOpenCVActor&); \
public:


#define OpenCVs_Source_OpenCVs_OpenCVActor_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOpenCVActor(AOpenCVActor&&); \
	NO_API AOpenCVActor(const AOpenCVActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOpenCVActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOpenCVActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AOpenCVActor)


#define OpenCVs_Source_OpenCVs_OpenCVActor_h_21_PRIVATE_PROPERTY_OFFSET
#define OpenCVs_Source_OpenCVs_OpenCVActor_h_18_PROLOG
#define OpenCVs_Source_OpenCVs_OpenCVActor_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OpenCVs_Source_OpenCVs_OpenCVActor_h_21_PRIVATE_PROPERTY_OFFSET \
	OpenCVs_Source_OpenCVs_OpenCVActor_h_21_SPARSE_DATA \
	OpenCVs_Source_OpenCVs_OpenCVActor_h_21_RPC_WRAPPERS \
	OpenCVs_Source_OpenCVs_OpenCVActor_h_21_INCLASS \
	OpenCVs_Source_OpenCVs_OpenCVActor_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define OpenCVs_Source_OpenCVs_OpenCVActor_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OpenCVs_Source_OpenCVs_OpenCVActor_h_21_PRIVATE_PROPERTY_OFFSET \
	OpenCVs_Source_OpenCVs_OpenCVActor_h_21_SPARSE_DATA \
	OpenCVs_Source_OpenCVs_OpenCVActor_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	OpenCVs_Source_OpenCVs_OpenCVActor_h_21_INCLASS_NO_PURE_DECLS \
	OpenCVs_Source_OpenCVs_OpenCVActor_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OPENCVS_API UClass* StaticClass<class AOpenCVActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID OpenCVs_Source_OpenCVs_OpenCVActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
