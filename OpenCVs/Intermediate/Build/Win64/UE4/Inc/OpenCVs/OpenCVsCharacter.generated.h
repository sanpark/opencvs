// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPENCVS_OpenCVsCharacter_generated_h
#error "OpenCVsCharacter.generated.h already included, missing '#pragma once' in OpenCVsCharacter.h"
#endif
#define OPENCVS_OpenCVsCharacter_generated_h

#define OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_SPARSE_DATA
#define OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_RPC_WRAPPERS
#define OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOpenCVsCharacter(); \
	friend struct Z_Construct_UClass_AOpenCVsCharacter_Statics; \
public: \
	DECLARE_CLASS(AOpenCVsCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OpenCVs"), NO_API) \
	DECLARE_SERIALIZER(AOpenCVsCharacter)


#define OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_INCLASS \
private: \
	static void StaticRegisterNativesAOpenCVsCharacter(); \
	friend struct Z_Construct_UClass_AOpenCVsCharacter_Statics; \
public: \
	DECLARE_CLASS(AOpenCVsCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OpenCVs"), NO_API) \
	DECLARE_SERIALIZER(AOpenCVsCharacter)


#define OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOpenCVsCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOpenCVsCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOpenCVsCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOpenCVsCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOpenCVsCharacter(AOpenCVsCharacter&&); \
	NO_API AOpenCVsCharacter(const AOpenCVsCharacter&); \
public:


#define OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOpenCVsCharacter(AOpenCVsCharacter&&); \
	NO_API AOpenCVsCharacter(const AOpenCVsCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOpenCVsCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOpenCVsCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AOpenCVsCharacter)


#define OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AOpenCVsCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AOpenCVsCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(AOpenCVsCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(AOpenCVsCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(AOpenCVsCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AOpenCVsCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(AOpenCVsCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(AOpenCVsCharacter, L_MotionController); }


#define OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_17_PROLOG
#define OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_PRIVATE_PROPERTY_OFFSET \
	OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_SPARSE_DATA \
	OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_RPC_WRAPPERS \
	OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_INCLASS \
	OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_PRIVATE_PROPERTY_OFFSET \
	OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_SPARSE_DATA \
	OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_INCLASS_NO_PURE_DECLS \
	OpenCVs_Source_OpenCVs_OpenCVsCharacter_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OPENCVS_API UClass* StaticClass<class AOpenCVsCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID OpenCVs_Source_OpenCVs_OpenCVsCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
