// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef OPENCVS_OpenCVsProjectile_generated_h
#error "OpenCVsProjectile.generated.h already included, missing '#pragma once' in OpenCVsProjectile.h"
#endif
#define OPENCVS_OpenCVsProjectile_generated_h

#define OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_SPARSE_DATA
#define OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit);


#define OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit);


#define OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOpenCVsProjectile(); \
	friend struct Z_Construct_UClass_AOpenCVsProjectile_Statics; \
public: \
	DECLARE_CLASS(AOpenCVsProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OpenCVs"), NO_API) \
	DECLARE_SERIALIZER(AOpenCVsProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAOpenCVsProjectile(); \
	friend struct Z_Construct_UClass_AOpenCVsProjectile_Statics; \
public: \
	DECLARE_CLASS(AOpenCVsProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OpenCVs"), NO_API) \
	DECLARE_SERIALIZER(AOpenCVsProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOpenCVsProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOpenCVsProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOpenCVsProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOpenCVsProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOpenCVsProjectile(AOpenCVsProjectile&&); \
	NO_API AOpenCVsProjectile(const AOpenCVsProjectile&); \
public:


#define OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOpenCVsProjectile(AOpenCVsProjectile&&); \
	NO_API AOpenCVsProjectile(const AOpenCVsProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOpenCVsProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOpenCVsProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AOpenCVsProjectile)


#define OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(AOpenCVsProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(AOpenCVsProjectile, ProjectileMovement); }


#define OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_12_PROLOG
#define OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_SPARSE_DATA \
	OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_RPC_WRAPPERS \
	OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_INCLASS \
	OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_SPARSE_DATA \
	OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_INCLASS_NO_PURE_DECLS \
	OpenCVs_Source_OpenCVs_OpenCVsProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OPENCVS_API UClass* StaticClass<class AOpenCVsProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID OpenCVs_Source_OpenCVs_OpenCVsProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
