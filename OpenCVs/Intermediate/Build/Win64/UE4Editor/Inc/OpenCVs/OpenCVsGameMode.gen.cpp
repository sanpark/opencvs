// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OpenCVs/OpenCVsGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOpenCVsGameMode() {}
// Cross Module References
	OPENCVS_API UClass* Z_Construct_UClass_AOpenCVsGameMode_NoRegister();
	OPENCVS_API UClass* Z_Construct_UClass_AOpenCVsGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_OpenCVs();
// End Cross Module References
	void AOpenCVsGameMode::StaticRegisterNativesAOpenCVsGameMode()
	{
	}
	UClass* Z_Construct_UClass_AOpenCVsGameMode_NoRegister()
	{
		return AOpenCVsGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AOpenCVsGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AOpenCVsGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_OpenCVs,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOpenCVsGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "OpenCVsGameMode.h" },
		{ "ModuleRelativePath", "OpenCVsGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AOpenCVsGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AOpenCVsGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AOpenCVsGameMode_Statics::ClassParams = {
		&AOpenCVsGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AOpenCVsGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AOpenCVsGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AOpenCVsGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AOpenCVsGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AOpenCVsGameMode, 2594581222);
	template<> OPENCVS_API UClass* StaticClass<AOpenCVsGameMode>()
	{
		return AOpenCVsGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AOpenCVsGameMode(Z_Construct_UClass_AOpenCVsGameMode, &AOpenCVsGameMode::StaticClass, TEXT("/Script/OpenCVs"), TEXT("AOpenCVsGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AOpenCVsGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
