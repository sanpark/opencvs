// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPENCVS_OpenCVsGameMode_generated_h
#error "OpenCVsGameMode.generated.h already included, missing '#pragma once' in OpenCVsGameMode.h"
#endif
#define OPENCVS_OpenCVsGameMode_generated_h

#define OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_SPARSE_DATA
#define OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_RPC_WRAPPERS
#define OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOpenCVsGameMode(); \
	friend struct Z_Construct_UClass_AOpenCVsGameMode_Statics; \
public: \
	DECLARE_CLASS(AOpenCVsGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/OpenCVs"), OPENCVS_API) \
	DECLARE_SERIALIZER(AOpenCVsGameMode)


#define OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAOpenCVsGameMode(); \
	friend struct Z_Construct_UClass_AOpenCVsGameMode_Statics; \
public: \
	DECLARE_CLASS(AOpenCVsGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/OpenCVs"), OPENCVS_API) \
	DECLARE_SERIALIZER(AOpenCVsGameMode)


#define OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OPENCVS_API AOpenCVsGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOpenCVsGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OPENCVS_API, AOpenCVsGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOpenCVsGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OPENCVS_API AOpenCVsGameMode(AOpenCVsGameMode&&); \
	OPENCVS_API AOpenCVsGameMode(const AOpenCVsGameMode&); \
public:


#define OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OPENCVS_API AOpenCVsGameMode(AOpenCVsGameMode&&); \
	OPENCVS_API AOpenCVsGameMode(const AOpenCVsGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OPENCVS_API, AOpenCVsGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOpenCVsGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AOpenCVsGameMode)


#define OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_9_PROLOG
#define OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_SPARSE_DATA \
	OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_RPC_WRAPPERS \
	OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_INCLASS \
	OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_SPARSE_DATA \
	OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_INCLASS_NO_PURE_DECLS \
	OpenCVs_Source_OpenCVs_OpenCVsGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OPENCVS_API UClass* StaticClass<class AOpenCVsGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID OpenCVs_Source_OpenCVs_OpenCVsGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
