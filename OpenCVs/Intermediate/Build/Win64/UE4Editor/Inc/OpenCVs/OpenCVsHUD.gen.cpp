// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OpenCVs/OpenCVsHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOpenCVsHUD() {}
// Cross Module References
	OPENCVS_API UClass* Z_Construct_UClass_AOpenCVsHUD_NoRegister();
	OPENCVS_API UClass* Z_Construct_UClass_AOpenCVsHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_OpenCVs();
// End Cross Module References
	void AOpenCVsHUD::StaticRegisterNativesAOpenCVsHUD()
	{
	}
	UClass* Z_Construct_UClass_AOpenCVsHUD_NoRegister()
	{
		return AOpenCVsHUD::StaticClass();
	}
	struct Z_Construct_UClass_AOpenCVsHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AOpenCVsHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_OpenCVs,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOpenCVsHUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "OpenCVsHUD.h" },
		{ "ModuleRelativePath", "OpenCVsHUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AOpenCVsHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AOpenCVsHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AOpenCVsHUD_Statics::ClassParams = {
		&AOpenCVsHUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AOpenCVsHUD_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AOpenCVsHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AOpenCVsHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AOpenCVsHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AOpenCVsHUD, 727914133);
	template<> OPENCVS_API UClass* StaticClass<AOpenCVsHUD>()
	{
		return AOpenCVsHUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AOpenCVsHUD(Z_Construct_UClass_AOpenCVsHUD, &AOpenCVsHUD::StaticClass, TEXT("/Script/OpenCVs"), TEXT("AOpenCVsHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AOpenCVsHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
