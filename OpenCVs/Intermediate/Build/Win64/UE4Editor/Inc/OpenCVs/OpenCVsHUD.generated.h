// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPENCVS_OpenCVsHUD_generated_h
#error "OpenCVsHUD.generated.h already included, missing '#pragma once' in OpenCVsHUD.h"
#endif
#define OPENCVS_OpenCVsHUD_generated_h

#define OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_SPARSE_DATA
#define OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_RPC_WRAPPERS
#define OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOpenCVsHUD(); \
	friend struct Z_Construct_UClass_AOpenCVsHUD_Statics; \
public: \
	DECLARE_CLASS(AOpenCVsHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/OpenCVs"), NO_API) \
	DECLARE_SERIALIZER(AOpenCVsHUD)


#define OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAOpenCVsHUD(); \
	friend struct Z_Construct_UClass_AOpenCVsHUD_Statics; \
public: \
	DECLARE_CLASS(AOpenCVsHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/OpenCVs"), NO_API) \
	DECLARE_SERIALIZER(AOpenCVsHUD)


#define OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOpenCVsHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOpenCVsHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOpenCVsHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOpenCVsHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOpenCVsHUD(AOpenCVsHUD&&); \
	NO_API AOpenCVsHUD(const AOpenCVsHUD&); \
public:


#define OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOpenCVsHUD(AOpenCVsHUD&&); \
	NO_API AOpenCVsHUD(const AOpenCVsHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOpenCVsHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOpenCVsHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AOpenCVsHUD)


#define OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define OpenCVs_Source_OpenCVs_OpenCVsHUD_h_9_PROLOG
#define OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_SPARSE_DATA \
	OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_RPC_WRAPPERS \
	OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_INCLASS \
	OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_SPARSE_DATA \
	OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_INCLASS_NO_PURE_DECLS \
	OpenCVs_Source_OpenCVs_OpenCVsHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OPENCVS_API UClass* StaticClass<class AOpenCVsHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID OpenCVs_Source_OpenCVs_OpenCVsHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
