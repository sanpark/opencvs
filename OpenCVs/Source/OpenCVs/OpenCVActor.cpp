


#include "OpenCVActor.h"
#include "OpenCVs.h"
#include "Runtime/Engine/Classes/Engine/TextureRenderTarget2D.h"
#include "Engine/Classes/Components/SphereComponent.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Engine/Texture2D.h"
#include "HighResScreenshot.h"
#include "Runtime/Engine/Public/HighResScreenshot.h"
#include "ImageUtils.h"


//#include "Modules/ModuleManager.h"

//IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, OpenCVs, "OpenCVs");
//DEFINE_LOG_CATEGORY(LogMyGame);



using namespace cv;


int cv_template_matching(Mat screenSrc)
{
	double minVal, maxVal;
	Point minLoc, maxLoc;
	Point matchLoc;

	Mat src = screenSrc;
	if (src.empty())
	{
		return -1;
	}
	FString path = FPaths::ConvertRelativePathToFull(*FPaths::ProjectContentDir());

	UE_LOG(LogTemp, Log, TEXT("[SAN] %s"), &path);
	//path = FPaths::Combine(path, "lenna.jpg");
	path = path + "lenna.jpg";
	std::string cstr = std::string(TCHAR_TO_UTF8(*path));
	Mat templ = imread(cstr);
	if (src.empty())
	{
		return -1;
	}
	Mat result;

	for (int i = 0; i < 6; i++)
	{
		Mat img_out;
		src.copyTo(img_out);

		int Matching_method = i;
		/*
		0: TM_SQDIFF
		1: TM_SQDIFF NORMED
		2: TM CCORR
		3: TM CCORR NORMED
		4: TM COEFF
		5: TM COEFF NORMED";
		*/

		matchTemplate(src, templ, result, i);
		normalize(result, result, 0, 1, NORM_MINMAX, -1, Mat());
		minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
		if (Matching_method == 0 || Matching_method == 1)
		{
			matchLoc = minLoc;
		}
		else
			matchLoc = maxLoc;

		//cout << "Min value : " << minVal << endl;
		//cout << "Max value : " << maxVal << endl;

		rectangle(img_out, matchLoc, Point(matchLoc.x + templ.cols, matchLoc.y + templ.rows), Scalar(0, 0, 255), 1);

		cvtColor(result, result, COLOR_GRAY2BGR);
		circle(result, matchLoc, 10, Scalar(0, 0, 255), 1);

		cv::imshow("src", img_out);
		cv::imshow("templ", templ);
		FString path2 = FPaths::ConvertRelativePathToFull(*FPaths::ProjectContentDir());

		UE_LOG(LogTemp, Log, TEXT("[SAN] %s"), &path2);
		//path = FPaths::Combine(path, "lenna.jpg");
		path2 = path2 + "/../";
		FString path3 = path2 + "screen_capture_"+ FString::FromInt(i)+".jpg";
		FString path4 = path2 + "template.jpg";

		path2 = path2 + "result_"+ FString::FromInt(i) +".jpg";
		std::string cstr2 = std::string(TCHAR_TO_UTF8(*path2));
		std::string cstr3 = std::string(TCHAR_TO_UTF8(*path3));

		
		std::string cstr4 = std::string(TCHAR_TO_UTF8(*path4));

		imwrite(cstr2, result);
		imwrite(cstr3, screenSrc);
		imwrite(cstr4, templ);
		cv::waitKey(0);

	}
	UE_LOG(LogTemp, Log, TEXT("[SAN] LOG5"));

	return 0;
}

void AOpenCVActor::SaveTextureColor()
{
	//auto RenderTargetResource = renderTargetColor->GameThread_GetRenderTargetResource();

	auto RenderTargetResource = TextureRenderTarget->GameThread_GetRenderTargetResource();
	if (RenderTargetResource)
	{
		TArray<FColor> buffer8;
		RenderTargetResource->ReadPixels(buffer8);
		Mat frame = Mat::zeros(1024, 1024, CV_8UC3);

		for (int j = 0; j < 1024; j++)
		{
			for (int i = 0; i < 1024; i++)
			{
				FColor pixels = buffer8.Pop();
				//frame.data[i * 3 + 0] = pixels.B;
				//frame.data[i * 3 + 1] = pixels.G;
				//frame.data[i * 3 + 2] = pixels.R;
				frame.at<Vec3b>(1024-j, 1024-i)[0] = pixels.B; // endian problem
				frame.at<Vec3b>(1024-j, 1024-i)[1] = pixels.G;
				frame.at<Vec3b>(1024-j, 1024-i)[2] = pixels.R;				
		
			}
		}
		
		cv_template_matching(frame);
	}
}

void AOpenCVActor::Released_F()
{
	SaveTextureColor();
	//GEngine->AddOnScreenDebugMessage(-1, 4, FColor::Blue, TEXT("Pressed F"));
	UE_LOG(LogTemp, Log, TEXT("[SAN] LOG4"));

}

// Sets default values
AOpenCVActor::AOpenCVActor()
{
	
	//InputComponent->BindKey(EKeys::F, IE_Released, this, &AOpenCVActor::Released_F);
	//파일과 에디터 로그에 출력된다.
	//UE_LOG(LogMyGame, Log, TEXT("Start OpenCV"));

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//sampleInputImage = imread("input.png");

	UE_LOG(LogTemp, Log, TEXT("[SAN] LOG3"));
	
}

void AOpenCVActor::SaveRenderTargetToDisk(UTextureRenderTarget2D* InRenderTarget, FString Filename)
{
	FTextureRenderTargetResource* RTResource = InRenderTarget->GameThread_GetRenderTargetResource();

	FReadSurfaceDataFlags ReadPixelFlags(RCM_UNorm);
	ReadPixelFlags.SetLinearToGamma(true);

	TArray<FColor> OutBMP;
	RTResource->ReadPixels(OutBMP, ReadPixelFlags);

	for (FColor& color : OutBMP)
	{
		color.A = 255;
	}

	//FIntRect SourceRect;
	//FIntPoint DestSize(InRenderTarget->GetSurfaceWidth(), InRenderTarget->GetSurfaceHeight());
	//FString ResultPath;
	//FHighResScreenshotConfig& HighResScreenshotConfig = GetHighResScreenshotConfig();	
	//HighResScreenshotConfig.SaveImage(Filename, OutBMP, DestSize, &ResultPath);
}

// Called when the game starts or when spawned
void AOpenCVActor::BeginPlay()
{
	Super::BeginPlay();
	InputComponent->BindKey(EKeys::F, IE_Released, this, &AOpenCVActor::Released_F);

	PrimaryActorTick.bCanEverTick = true;
	UE_LOG(LogTemp, Log, TEXT("[SAN] LOG1"));
	SceneCapture->TextureTarget = TextureRenderTarget;
	SceneCapture->bCaptureEveryFrame = true;
	Texture2D = SceneCapture->TextureTarget->ConstructTexture2D(this, "CameraImage", EObjectFlags::RF_NoFlags, CTF_Default);//CTF_DeferCompression);
	TextureRenderTarget->UpdateResourceImmediate();

}

// Called every frame
void AOpenCVActor::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);
	//UE_LOG(LogTemp, Log, TEXT("[SAN] LOG2"));

	SceneCapture->CaptureScene();
	SceneCapture->UpdateContent();
}

