// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"    
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/InputComponent.h"

#include "Components/SceneCaptureComponent2D.h"
#include "Engine/Classes/Components/SphereComponent.h"

#include "OpenCVActor.generated.h"

UCLASS(Blueprintable)
class OPENCVS_API AOpenCVActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AOpenCVActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	USceneCaptureComponent2D *Camera;
	UTextureRenderTarget2D *RenderTarget;
	UTexture2D *Texture2D;
	//ConstructorHelpers::FObjectFinder<UTextureRenderTarget2D> RenderTargetAsset;


	// Blueprint Event called every time the video frame is updated
	//UFUNCTION(BlueprintImplementableEvent, Category = None)
	void SaveTextureColor();


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USceneCaptureComponent2D *SceneCapture;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTextureRenderTarget2D *TextureRenderTarget;

	//UFUNCTION(BlueprintCallable)
	//	UTextureRenderTarget2D* CreateRenderTarget2D(int32 width, int32 height, bool makeHDR);

	//UFUNCTION(BlueprintCallable)
	//	bool SaveRenderTarget(UTextureRenderTarget2D* renderTarget, FString path, FString fileName);


	// OpenCV fields
	//cv::Mat* frame;
	cv::Mat sampleInputImage;

	cv::VideoCapture stream;
	cv::Size size;

	UFUNCTION(BlueprintCallable)
	void Released_F();

	// If the stream has succesfully opened yet
	UPROPERTY(BlueprintReadOnly, Category = None)
		bool isStreamOpen;

	// The videos width and height (width, height)
	UPROPERTY(BlueprintReadWrite, Category = None)
		FVector2D VideoSize;

	// The current video frame's corresponding texture
	UPROPERTY(BlueprintReadOnly, Category = None)
		UTexture2D* VideoTexture;

	// The current data array
	UPROPERTY(BlueprintReadOnly, Category = None)
		TArray<FColor> Data;

protected:

	// Use this function to update the texture rects you want to change:
	// NOTE: There is a method called UpdateTextureRegions in UTexture2D but it is compiled WITH_EDITOR and is not marked as ENGINE_API so it cannot be linked
	// from plugins.
	// FROM: https://wiki.unrealengine.com/Dynamic_Textures
	void SaveRenderTargetToDisk(UTextureRenderTarget2D* InRenderTarget, FString Filename);

	// Pointer to update texture region 2D struct
	FUpdateTextureRegion2D* VideoUpdateTextureRegion;
};