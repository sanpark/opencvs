// Copyright Epic Games, Inc. All Rights Reserved.
using System.IO;

using UnrealBuildTool;

public class OpenCVs : ModuleRules
{
    string OPENCV_VERSION = "320";
    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/")); }

    }
    //public bool LoadOpenCV(ReadOnlyTargetRules Target)
    //{
    //    // Start OpenCV linking here!
    //    bool isLibrarySupported = false;

    //    // Create OpenCV Path 
    //    string OpenCVPath = Path.Combine(ThirdPartyPath, "OpenCV");

    //    // Get Library Path 
    //    string LibPath = "";
    //    //bool isdebug = Target.Configuration == UnrealTargetConfiguration.Debug && BuildConfiguration.bDebugBuildsActuallyUseDebugCRT;
    //    bool isdebug = false;
    //    if (Target.Platform == UnrealTargetPlatform.Win64)
    //    {
    //        LibPath = Path.Combine(OpenCVPath, "Libraries", "Win64");
    //        isLibrarySupported = true;
    //    }
    //    else
    //    {
    //        string Err = string.Format("{0} dedicated server is made to depend on {1}. We want to avoid this, please correct module dependencies.", Target.Platform.ToString(), this.ToString()); System.Console.WriteLine(Err);
    //    }

    //    if (isLibrarySupported)
    //    {
    //        //Add Include path 
    //        PublicIncludePaths.AddRange(new string[] { Path.Combine(OpenCVPath, "Includes") });

    //        // Add Library Path 
    //        PublicLibraryPaths.Add(LibPath);

    //        //Add Static Libraries
    //        //PublicAdditionalLibraries.Add("../../ThirdParty/OpenCV/Libraries/Win64/opencv_world320.lib");

    //        PublicAdditionalLibraries.Add("opencv_world320.lib");

    //        //Add Dynamic Libraries
    //        PublicDelayLoadDLLs.Add("opencv_world320.dll");
    //        PublicDelayLoadDLLs.Add("opencv_ffmpeg320_64.dll");
    //    }

    //    Definitions.Add(string.Format("WITH_OPENCV_BINDING={0}", isLibrarySupported ? 1 : 0));

    //    return isLibrarySupported;
    //}
    public bool LoadOpenCV(ReadOnlyTargetRules Target)
    {
        // Start OpenCV linking
        bool isLibrarySupported = false;// Create OpenCV Path
        string OpenCVPath = Path.Combine(ThirdPartyPath, "OpenCV");// Get Library Path
        string LibPath = "";
        bool isdebug = Target.Configuration == UnrealTargetConfiguration.Debug;
        if (Target.Platform == UnrealTargetPlatform.Win64)
        {
            LibPath = Path.Combine(OpenCVPath, "Libraries", "Win64");
            isLibrarySupported = true;
        }
        else
        {
            string Err = string.Format("{0} dedicated server is made to depend on {1}. We want to avoid this, please correct module dependencies.", Target.Platform.ToString(), this.ToString());
            System.Console.WriteLine(Err);
        }
        if (isLibrarySupported)
        {
            //Add Include path
            PublicIncludePaths.AddRange(new string[] { Path.Combine(OpenCVPath, "Includes") });
            //Add Static Libraries
            PublicAdditionalLibraries.Add(Path.Combine(LibPath, "opencv_world" + OPENCV_VERSION + ".lib"));
            //Add Dynamic Libraries
            PublicDelayLoadDLLs.Add("opencv_world" + OPENCV_VERSION + ".dll");
            PublicDelayLoadDLLs.Add("opencv_videoio_ffmpeg" + OPENCV_VERSION + "_64.dll");
        }
        PublicDefinitions.Add(string.Format("WITH_OPENCV_BINDING={0}", isLibrarySupported ? 1 : 0));
        return isLibrarySupported;
    }


    public OpenCVs(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        //PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "RHI", "RenderCore", "ShaderCore" });
        PublicDependencyModuleNames.AddRange(new string[] { "Core", "Slate","SlateCore", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
        LoadOpenCV(Target);
    }



}
