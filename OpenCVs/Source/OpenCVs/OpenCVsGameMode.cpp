// Copyright Epic Games, Inc. All Rights Reserved.

#include "OpenCVsGameMode.h"
#include "OpenCVsHUD.h"
#include "OpenCVsCharacter.h"
#include "UObject/ConstructorHelpers.h"

AOpenCVsGameMode::AOpenCVsGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AOpenCVsHUD::StaticClass();
}
