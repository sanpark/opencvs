// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "OpenCVsGameMode.generated.h"

UCLASS(minimalapi)
class AOpenCVsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AOpenCVsGameMode();
};



