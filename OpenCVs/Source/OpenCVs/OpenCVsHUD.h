// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "OpenCVsHUD.generated.h"

UCLASS()
class AOpenCVsHUD : public AHUD
{
	GENERATED_BODY()

public:
	AOpenCVsHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

